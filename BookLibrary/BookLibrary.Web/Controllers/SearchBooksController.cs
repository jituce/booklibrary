﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BookLibrary.Data.Models;
using BookLibrary.Data.Repositories;

namespace BookLibrary.Web.Controllers
{
    /// <summary>
    /// Search Books Controller
    /// </summary>
    public class SearchBooksController : Controller
    {
        private IBookRepository _bookrepository;

        public SearchBooksController(IBookRepository bookrepository)
        {
            this._bookrepository = bookrepository;

        }

        // GET: SearchBooks
        public async Task<ActionResult> Index()
        {
            return View();
        }


        [HttpPost]
        public async Task<ActionResult> Find(string query)
        {
            try
            {
                ViewBag.CurrentFilter = query;

                if (!string.IsNullOrEmpty(query))
                {
                    string strSearchBy = Request.Form["searchBy"];
                    Session["SearchBy"] = ViewBag.SearchBy = strSearchBy;
                    if (strSearchBy == "Author" && !string.IsNullOrWhiteSpace(query))
                    {
                        IEnumerable<Book> books = await _bookrepository.GetBooksByAuthor(query);
                        return View("Index", books);
                    }
                    else if (strSearchBy == "BookName" && !string.IsNullOrWhiteSpace(query))
                    {
                        IEnumerable<Book> books = await _bookrepository.GetBooksByName(query);
                        return View("Index", books);
                    }

                }

                var all_books = await _bookrepository.GetBooks();
                return View("Index", all_books);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

        // GET: Books/Delete/5
        public async Task<ActionResult> Lend(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var book = _bookrepository.GetBookByID((int)id);

            Task result;

            Task continuation = book.ContinueWith(x => result = _bookrepository.LendBook(x.Result));

            var all_books = _bookrepository.GetBooks();

            await Task.WhenAll(continuation, all_books);

            if (book == null)
            {
                return HttpNotFound();
            }

            return View("Index", all_books.Result);

        }

        // GET: Books/Delete/5
        public async Task<ActionResult> Return(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var book = _bookrepository.GetBookByID((int)id);

            Task result;

            Task continuation =  book.ContinueWith(x => result = _bookrepository.ReturnBook(x.Result));
          

            var all_books = await _bookrepository.GetBooks();


            if (book == null)
            {
                return HttpNotFound();
            }

            return View("Index", all_books);

        }
    }

}
