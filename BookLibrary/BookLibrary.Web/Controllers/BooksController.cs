﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BookLibrary.Data.Models;
using BookLibrary.Data.Repositories;

namespace BookLibrary.Web.Controllers
{
    /// <summary>
    /// Books controller    
    /// </summary>
    public class BooksController : Controller
    {
        private IBookRepository _bookRepository;
        private IAuthorRepository _authorRepository;
        private ICategoryRepository _categoryRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="BooksController"/> class.
        /// </summary>
        /// <param name="bookRepository">The book repository.</param>
        /// <param name="authorRepository">The author repository.</param>
        /// <param name="CategoryRepository">The category repository.</param>
        public BooksController(IBookRepository bookRepository, IAuthorRepository authorRepository, ICategoryRepository CategoryRepository)
        {
            this._bookRepository = bookRepository;
            this._authorRepository = authorRepository;
            this._categoryRepository =CategoryRepository;
        }

        // GET: Books
        public async Task<ActionResult> Index()
        {
            IEnumerable<Book> books = await _bookRepository.GetBooks();            
            return View(books);
        }

        // GET: Books/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = await _bookRepository.GetBookByID((int)id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        // GET: Books/Create
        public async Task<ActionResult> Create()
        {
            IEnumerable<Author> authors = await _authorRepository.GetAuthors();
            IEnumerable<Category> categories = await _categoryRepository.GetCategories();

            ViewBag.AuthorID = authors;
            ViewBag.CategoryID = categories;
            return View();
        }

        // POST: Books/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Book book)
        {
            if (ModelState.IsValid)
            {
                Author author = await _authorRepository.GetAuthorByID(book.AuthorId);
                book.Author = author;   

                var category = await _categoryRepository.GetCategoryByID(book.CategoryId);
                book.Category = category;

                await _bookRepository.InsertBook(book);
                return RedirectToAction("Index");
            }

            return View(book);
        }

        // GET: Books/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IEnumerable<Author> authors = await _authorRepository.GetAuthors();
            IEnumerable<Category> categories = await _categoryRepository.GetCategories();

            Book book = await _bookRepository.GetBookByID((int)id);
            ViewBag.AuthorID = new SelectList(authors, "Id", "FullName", book.AuthorId);
            ViewBag.CategoryID = new SelectList(categories, "Id", "Name", book.CategoryId);
            
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        // POST: Books/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit( Book book)
        {
            if (ModelState.IsValid)
            {
                await _bookRepository.UpdateBook(book);
                return RedirectToAction("Index");
            }
            return View(book);
        }

        // GET: Books/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = await _bookRepository.GetBookByID((int)id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        // POST: Books/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            await _bookRepository.DeleteBook(id);
            return RedirectToAction("Index");
        }
    }
}
