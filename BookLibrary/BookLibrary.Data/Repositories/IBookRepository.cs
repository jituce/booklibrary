﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookLibrary.Data.Models;

namespace BookLibrary.Data.Repositories
{
    /// <summary>
    /// Book repository
    /// </summary>
    public interface IBookRepository
    {
        Task<IEnumerable<Book>> GetBooks();
        Task<Book> GetBookByID(int bookId);
        Task InsertBook(Book book);
        Task DeleteBook(int bookID);
        Task UpdateBook(Book book);
        Task LendBook(Book book);
        Task ReturnBook(Book book);
        Task<IEnumerable<Book>> GetBooksByName(string name);
        Task<IEnumerable<Book>> GetBooksByAuthor(string name);
    }
}
