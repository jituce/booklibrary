﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookLibrary.Data.Models;

namespace BookLibrary.Data.Repositories
{
    /// <summary>
    /// Category Repository
    /// </summary>
    public class CategoryRepository : ICategoryRepository, IDisposable
    {
        /// <summary>
        /// The context
        /// </summary>
        private BookLibraryEntities context;

        /// <summary>
        /// Initializes a new instance of the <see cref="CategoryRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public CategoryRepository(BookLibraryEntities context)
        {
            this.context = context;
        }

        /// <summary>
        /// Gets the categories.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Category>> GetCategories()
        {
            return await context.Categories.OrderBy(c => c.Name).Where(l => l.Deleted == false).ToListAsync();
        }

        /// <summary>
        /// Gets the category by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public async Task<Category> GetCategoryByID(int id)
        {
            return await context.Categories.FindAsync(id);
        }

        /// <summary>
        /// Inserts the category.
        /// </summary>
        /// <param name="category">The category.</param>
        /// <returns></returns>
        public async Task InsertCategory(Category category)
        {
            category.Deleted = false;
            context.Categories.Add(category);
            await context.SaveChangesAsync();
        }

        /// <summary>
        /// Deletes the category.
        /// </summary>
        /// <param name="categoryID">The category identifier.</param>
        /// <returns></returns>
        public async Task DeleteCategory(int categoryID)
        {
            Category category = context.Categories.Find(categoryID);
            category.Deleted = true;
            context.Entry(category).State = EntityState.Modified;
            await context.SaveChangesAsync();
        }

        /// <summary>
        /// Updates the category.
        /// </summary>
        /// <param name="category">The category.</param>
        /// <returns></returns>
        public async Task UpdateCategory(Category category)
        {
            context.Entry(category).State = EntityState.Modified;
            await context.SaveChangesAsync();
        }

        /// <summary>
        /// The disposed
        /// </summary>
        private bool disposed = false;

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
