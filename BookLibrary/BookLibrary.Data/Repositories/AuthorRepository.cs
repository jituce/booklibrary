﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookLibrary.Data.Models;

namespace BookLibrary.Data.Repositories
{
    /// <summary>
    /// Authors Repository
    /// </summary>
    public class AuthorRepository : IAuthorRepository, IDisposable
    {
        /// <summary>
        /// The context
        /// </summary>
        private BookLibraryEntities context;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public AuthorRepository(BookLibraryEntities context)
        {
            this.context = context;
        }

        /// <summary>
        /// Gets the authors.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Author>> GetAuthors()
        {
            return await context.Authors.OrderBy(a => a.LastName).Where(l => l.Deleted == false).ToListAsync();
        }

        /// <summary>
        /// Gets the author by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public async Task<Author> GetAuthorByID(int id)
        {
            return await context.Authors.FindAsync(id);
        }

        /// <summary>
        /// Inserts the author.
        /// </summary>
        /// <param name="author">The author.</param>
        /// <returns></returns>
        public async Task InsertAuthor(Author author)
        {
            author.Deleted = false;
            context.Authors.Add(author);
            await context.SaveChangesAsync();
        }

        /// <summary>
        /// Deletes the author.
        /// </summary>
        /// <param name="authorID">The author identifier.</param>
        /// <returns></returns>
        public async Task DeleteAuthor(int authorID)
        {
            Author author = context.Authors.Find(authorID);
            author.Deleted = true;
            context.Entry(author).State = EntityState.Modified;
            await context.SaveChangesAsync();
        }

        /// <summary>
        /// Updates the author.
        /// </summary>
        /// <param name="author">The author.</param>
        /// <returns></returns>
        public async Task UpdateAuthor(Author author)
        {
            context.Entry(author).State = EntityState.Modified;
            await context.SaveChangesAsync();
        }

        /// <summary>
        /// The disposed
        /// </summary>
        private bool disposed = false;

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
