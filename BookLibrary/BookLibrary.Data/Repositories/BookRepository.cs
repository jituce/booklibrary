﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookLibrary.Data.Models;

namespace BookLibrary.Data.Repositories
{
    /// <summary>
    /// Book Repository
    /// </summary>
    public class BookRepository : IBookRepository, IDisposable
    {
        /// <summary>
        /// The context
        /// </summary>
        private readonly BookLibraryEntities context;

        /// <summary>
        /// Initializes a new instance of the <see cref="BookRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public BookRepository(BookLibraryEntities context)
        {
            this.context = context;
        }

        /// <summary>
        /// Gets the books.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Book>> GetBooks()
        {
            var context2 = new BookLibraryEntities();
            return await context2.Books.OrderBy(b => b.Name).ToListAsync();
        }

        /// <summary>
        /// Gets the book by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public async Task<Book> GetBookByID(int id)
        {
            return await context.Books.FindAsync(id);
        }

        /// <summary>
        /// Inserts the book.
        /// </summary>
        /// <param name="book">The book.</param>
        /// <returns></returns>
        public async Task InsertBook(Book book)
        {
            context.Books.Add(book);
            await context.SaveChangesAsync();
        }

        /// <summary>
        /// Deletes the book.
        /// </summary>
        /// <param name="bookID">The book identifier.</param>
        /// <returns></returns>
        public async Task DeleteBook(int bookID)
        {
            Book book = context.Books.Find(bookID);
            book.Author = null;
            book.Category = null;
            context.Books.Remove(book);
            await context.SaveChangesAsync();
        }

        /// <summary>
        /// Updates the book.
        /// </summary>
        /// <param name="book">The book.</param>
        /// <returns></returns>
        public async Task UpdateBook(Book book)
        {
            context.Entry(book).State = EntityState.Modified;
            await context.SaveChangesAsync();
        }

        /// <summary>
        /// Lend the book.
        /// </summary>
        /// <param name="book">The book.</param>
        /// <returns></returns>
        public async Task LendBook(Book book)
        {
            if (book.Stock > 0)
                book.Stock--;
            else
                book.Stock = 0;
            context.Entry(book).State = EntityState.Modified;
            await context.SaveChangesAsync();
        }

        /// <summary>
        /// Return the book.
        /// </summary>
        /// <param name="book">The book.</param>
        /// <returns></returns>
        public async Task ReturnBook(Book book)
        {
            book.Stock++;
            context.Entry(book).State = EntityState.Modified;
            await context.SaveChangesAsync();
        }

        /// <summary>
        /// Gets the books by category.
        /// </summary>
        /// <param name="categoryID">The category identifier.</param>
        /// <returns></returns>
        public async Task<IEnumerable<Book>> GetBooksByCategory(int categoryID)
        {
            return await context.Books.Include(b => b.Category).Where(c => c.Category.Id == categoryID).ToListAsync();
        }

        /// <summary>
        /// Gets the books by author.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public async Task<IEnumerable<Book>> GetBooksByAuthor(string name)
        {
            return await context.Books.Where(c => c.Author.LastName.Contains(name)
                                            || c.Author.FirstName.Contains(name)
                                            || name == null)
                                        .ToListAsync();
        }

        /// <summary>
        /// Gets the books by Name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public async Task<IEnumerable<Book>> GetBooksByName(string name)
        {
            return await context.Books.Where(c => c.Name.Contains(name)
                                            || name == null)
                                        .ToListAsync();
        }

        /// <summary>
        /// The disposed
        /// </summary>
        private bool disposed = false;

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
